<?php session_start();
/*
//Project Name: GCU Student Blog
//Version 1.5
//Module: Registration Handler Version 1
//Version 1.1: Added function to connect to database. Added registerFailed.php functionality.
//             Added features to validate input.
//Version 1.2: Added query to check if email address already exists in database.
Version 1.5 Updates: Changed Insert SQL statement to a prepared statement.
//Programmers: Robbie Evans III, Michael Rogers
//Date: 9/10/2017
//Synopsis: Retrieves input from registration.php. Inserts data into blogusers database.
//Requires registration.php, utility/dbFunctions.php, utility/phpFunctions.php,
//         registerFailed.php, registerSuccess.php
*/

require_once('../utility/dbFunctions.php');
require_once('../utility/phpFunctions.php');

$firstName = $_POST['firstName'];
$lastName = $_POST['lastName'];
$dateOfBirth = $_POST['DOB'];
$emailAddress = $_POST['emailAddress'];
$password = $_POST['password'];
$passwordConfirm = $_POST['passwordTwo'];


//Establish connection to database
$mysqli = dbConnect();

//Check if requested email address exists in database
$emailCheck = $mysqli->query ("SELECT * FROM userinfo WHERE EmailAddress = '$emailAddress'");
//Return number of rows found
$count = $emailCheck->num_rows;
if ($count > 0)
{
    $message = "This email address has already been registered. Please use a different email.";
    include('registerFailed.php');
}
else
{

    //Check if any fields are empty
    if (validateInput($firstName)) {
        $message = "The First Name is a required field and cannot be blank.";
        include('registerFailed.php');
    } elseif (validateInput($lastName)) {
        $message = "The Last Name is a required field and cannot be blank.";
        include('registerFailed.php');
    } elseif (validateInput($dateOfBirth)) {
        $message = "Date of Birth is a required field and cannot be blank.";
        include('registerFailed.php');
    }
    //Verifies that input is a valid email format.
    elseif (validateInput($emailAddress) || !filter_var($emailAddress, FILTER_VALIDATE_EMAIL)) {
        if (validateInput($emailAddress)) {
            $message = "The Email Address is a required field and cannot be blank.";
            include('registerFailed.php');
        } else {
            $message = "Invalid email address format.";
            include('registerFailed.php');
        }
    }
    //Verifies that password meets the length requirements
    elseif (validateInput($password) || strlen($password) < 6 || strlen($password) > 15) {
        if (validateInput($password)) {
            $message = "The Password is a required field and cannot be blank.";
            include('registerFailed.php');
        } elseif (strlen($password) < 6) {
            $message = "The Password must be greater than 5 characters.";
            include('registerFailed.php');
        } else {
            $message = "The Password must be less than 16 characters.";
            include('registerFailed.php');
        }
    }
    //Verify that both passwords match
    elseif ($password != $passwordConfirm) {
        $message = "Passwords do not match.";
        include('registerFailed.php');
    }
    else { //Insert data using prepared statement
        $stmt = $mysqli->prepare("INSERT into userinfo (FirstName, LastName, DOB, EmailAddress, Password)
                              VALUES (?, ?, STR_TO_DATE('$dateOfBirth', '%m/%d/%Y'), ?, ?)");
        $stmt->bind_param("ssss", $firstName, $lastName, $emailAddress, $password);
        $stmt->execute();
        $stmt->close();
        $message = "You are now registered.";
        include('registerSuccess.php');
    }
}
$mysqli->close();
?>
