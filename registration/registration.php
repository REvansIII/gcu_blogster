<!--
//Project Name: GCU Student Blog
//Version 1.5
//Module: Registration HTML Page Version 1
//Version 1.3 Updates: Converted from HTML to php extension. Added include of registrationHeader.php
//Version 1.5 Updates: Added (Required) tag to all fields.
//Programmers: Robbie Evans III, Michael Rogers
//Date: 9/10/2017
//Synopsis: Display fields for user to register for blog account. After input is validated, data is stored in MySQL db.
//Requires registrationHandler.php, login.html
-->


<?php
include('registrationHeader.php');
?>

<!-- Display Registration Form -->
<div class = "regForm">
<form id="regForm" action="registrationHandler.php" method="post" onsubmit="return checkForm()">
    <label>First Name (Required)</label><br>
    <input type="text" name="firstName" maxlength="20" required/><br><br>

    <label>Last Name (Required)</label><br>
    <input type="text" name="lastName" maxlength="20" required/><br><br>

    <label>Date of Birth (Required)</label><br>
    <input type="text" name="DOB" placeholder = "MM/DD/YYYY" maxlength="10" required/><br><br>

    <label>Email Address (Required)</label><br>
    <input type="text" name="emailAddress" maxlength="255" required/><br><br>

    <label>Password (Required)</label><br>
    <input type="password" name="password" placeholder = "(Between 6-15 characters)" id="passwordOne" minlength="6" maxlength="15" required/><br><br>

    <label>Re-Enter Password (Required)</label><br>
    <input type="password" name="passwordTwo" id="passwordTwo" minlength="6" maxlength="15" required/><br>

    <input type="reset" value="Clear Fields"><br>
    <input type="submit" value="Register"><br>
    Already registered?<br>
    <a href="../login/login.php">Click here to login.</a>

</form>
</div>


</body>
</html>