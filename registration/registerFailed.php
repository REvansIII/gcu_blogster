<?php session_start();

include('registrationHeader.php');
?>

<!--
//Project Name: GCU Student Blog
//Version 1.3
//Module: Registration Failed Version 1
//Programmers: Robbie Evans III, Michael Rogers
//Date: 8/20/2017
//Synopsis: Page to display registration failure message based on passed in variable.
-->

<h3><?php echo $message ?></h3><br>
<p style = "text-align: center">
<a href = "registration.php">Return to Registration Page.</a>
</p>



</body>
</html>