<?php session_start();

include('registrationHeader.php');
?>

<!--
//Project Name: GCU Student Blog
//Version 1.3
//Module: Registration Success Version 1
//Programmers: Robbie Evans III, Michael Rogers
//Date: 8/20/2017
//Synopsis: Page to display when registration has succeeded.
//Requires: login/login.php
-->


<h2><?php echo $message ?></h2><br>
<div style = "text-align: center">
<a href = "../login/login.php">Go To Login Page.</a>
</div>


</body>
</html>