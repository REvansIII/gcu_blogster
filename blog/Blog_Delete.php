<?php
include('blogHeader.php');
include('_blogMenu.php');
require_once('../utility/dbFunctions.php');

/*
//Project Name: GCU Student Blog
//Version 1.3
//Module: Blog Delete Page Version 1
//Programmers: Robbie Evans III, Michael Rogers
//Date: 8/20/2017
//Synopsis: Page to confirm user wishes to delete selected blog entry.
//Requires blogHeader.php, _blogMenu.php, utility/dbFunctions.php, deleteConfirm.php
*/



$ID = $_GET['BlogID'];

//Connect to DB
$mysqli = dbConnect();

//Get entry information from database
$blogPost = $mysqli->query ("SELECT * FROM entries WHERE BlogID = '$ID'");

//Display Title of Blog and ask user to confirm deletion.
foreach ($blogPost as $row) {
echo "Are you sure you want to delete " . $row['Title'] . "?<br> <br>";

    echo "<a href=\"deleteConfirm.php?BlogID=" . $row['BlogID'] . "\"> Delete </a>";
    echo "<a href=\"myBlogs.php\"> Do Not Delete </a>";
}

$mysqli->close();
