<?php session_start();

include('blogHeader.php');
include('_blogMenu.php');
require_once('../utility/dbFunctions.php');
require_once('../utility/phpFunctions.php');

/*
//Project Name: GCU Student Blog
//Version 1.5
//Module: Blog Entry List Version 1
//Version 1.5 Updates: Removed table generating code and replaced it with displayTable function.
//                     Added displayAllBlogs function.
//Programmers: Robbie Evans III, Michael Rogers
//Date: 9/10/2017
//Synopsis: Dislays list of all blogs in database
//Requires blogHeader.php, _blogMenu.php, Blog_Viewer.php. phpFunctions.php, dbFunctions.php
*/

// connection to database
$mysqli = dbConnect();

//Populate array of all blogs from database
$rows = displayAllBlogs();

//Display all blogs in a table
displayTable($rows);


$mysqli->close();

?>