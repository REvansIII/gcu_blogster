<!--
//Project Name: GCU Student Blog
//Version 1.3
//Module: Blog Header Page Version 1
//Programmers: Robbie Evans III, Michael Rogers
//Date: 8/20/2017
//Synopsis: Header page for use on various student portal pages
-->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>GCU Blog Login Page</title>
    <link rel="stylesheet" href="blogStyleSheet.css"/>
    <link rel="stylesheet" href="../style.css"/>

</head>

<body link="orange" vlink="#00bfff">
<div class="header">
    <img src="../logo.jpg" alt="logo" />
    <h1>GCU Student Blog</h1>
</div>