<?php session_start();

include('blogHeader.php');
include('_blogMenu.php');
require_once('../utility/phpFunctions.php');



//Project Name: GCU Student Blog
//Version 1.3
//Module: Blog Entry Page Version 1
//Version 1.3 Updates: Added includes of blogHeader.php, _blogMenu.php, and utility/phpFunctions.php
//Programmers: Robbie Evans III, Michael Rogers
//Date: 8/20/2017
//Synopsis: Page for user to enter blog entries
//Requires blogHandler.php, blogHeader.php, _blogMenu.php, utility/phpFunctions.php


//Display forms for user to input a new blog


 echo "<div class = 'blogForm'>";
    echo "<form id = 'blogForm' action = blogHandler.php method = 'post'>";
    echo "Title: <textarea name = 'title' rows = '1'  cols= '60' maxlength = '100' required/></textarea>";
    echo "Blog Entry: <textarea name = 'entry' rows = '20'  cols= '60' required/></textarea>";
    echo "<input type= 'submit' value= 'Submit'>";
    echo "</form>";
    echo "</div>";
