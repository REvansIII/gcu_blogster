<?php session_start();
include('blogHeader.php');
include('_blogMenu.php');
?>

<!--
//Project Name: GCU Student Blog
//Version 1.3
//Module: Blog Post Results Page Version 1
//Programmers: Robbie Evans III, Michael Rogers
//Date: 8/20/2017
//Synopsis: Page to display results of blog entry post attempts
//Requires blogHeader.php, _blogMenu.php
-->


<h2><?php echo $message ?></h2><br>
<a href="searchBlog.php"><p style="text-align:center">Back to Search</p></a>


</body>
</html>