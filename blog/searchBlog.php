<?php session_start();
include('blogHeader.php');
include('_blogMenu.php');
?>

<!--
//Project Name: GCU Student Blog
//Version 1.4
//Module: Blog Search Blogs Version 1
//Programmers: Robbie Evans III, Michael Rogers
//Date: 8/27/2017
//Synopsis: Search Blogs Page
//Requires blogHeader.php, _blogMenu.php, searchHandler.php
-->


<!-- Display form with user search fields-->
<div class = "searchForm">
    <form id="searchForm" action = searchHandler.php method="post">

        <label>Search by Author First Name</label><br>
        <input name="firstname" type="text" id="searchInput"  maxlength="20"/>
        <input name="Submit" value="Search" id="search" type="submit"/><br>

        <label>Search by Author Last Name</label><br>
        <input name="lastname" type="text" id="searchInput"  maxlength="20"/>
        <input name="Submit" value="Search" id="search" type="submit"/><br>

        <label>Search by Title</label><br>
        <input name="title" type="text" id="searchInput" maxlength="100"/>
        <input name="Submit" value="Search" id="search" type="submit"/><br>

        <label>Search by Date (YYYY-MM-DD)</label><br>
        <input name="date" type="text" id="searchInput" maxlength="10"/>
        <input name="Submit" value="Search" id="search" type="submit"/><br>


    </form>
</div>



</body>
</html>