<?php session_start();
/*
//Project Name: GCU Student Blog
//Version 1.5
//Module: Blog Handler Page Version 1
Version 1.3 Updates: Add include of utility/dbFunctions.php and postResults.php
Version 1.5 Updates: Changed SQL statement to a prepared statement.
//Programmers: Robbie Evans III, Michael Rogers
//Date: 9/10/2017
//Synopsis: Handler for posting blog entries.
//Requires blogHeader.php, _blogMenu.php, utility/dbFunctions.php, postResults.php
*/

require_once('../utility/dbFunctions.php');

$title = $_POST['title'];
$entry = $_POST['entry'];

//Connect to DB
$mysqli = dbConnect();

//Verify all fields contain input
if (strlen($_POST['title']) == null || strlen($_POST['entry']) == null)
{
    $message = "Error! Please ensure all fields have a value";
    include('postResults.php');
}

else
{ //Insert data using prepared statement
    $stmt = $mysqli->prepare("INSERT INTO entries (Title, Entry, Date, UserId) VALUES (?, ?, NOW(), ?)");
    $stmt->bind_param("sss", $title, $entry, $_SESSION["USER_ID"]);
    $stmt->execute();
    $stmt->close();
    $message = "Blog has been submitted.";
    include('postResults.php');

}

$mysqli->close();

?>