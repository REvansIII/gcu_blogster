<?php session_start();
/*
//Project Name: GCU Student Blog
//Version 1.5
//Module: Blog Update Handler Page Version 1
Version 1.5 Updates: Changed SQL statement to a prepared statement.
//Programmers: Robbie Evans III, Michael Rogers
//Date: 9/10/2017
//Synopsis: Handler for updating blog entries.
//Requires utility/dbFunctions.php, postResults.php
*/
require_once('../utility/dbFunctions.php');

$title = $_POST['title'];
$entry = $_POST['entry'];
$ID = $_POST['BlogId'];

//Connect to DB
$mysqli = dbConnect();

//Check if title or entry fields are null/empty, if not, posts update
if (strlen($_POST['title']) == null || strlen($_POST['entry']) < 1)
{
    $message = "Error! Please ensure all fields have a value";
    include('postResults.php');
}

else { //Update Blog Entry using prepared statement
    $stmt = $mysqli->prepare("UPDATE entries SET Title = ?, Entry = ? WHERE BlogID = '$ID'");
    $stmt->bind_param("ss", $title, $entry);
    $stmt->execute();
    $stmt->close();
    $message = "Blog has been updated.";
    include('postResults.php');
}

$mysqli->close();
?>