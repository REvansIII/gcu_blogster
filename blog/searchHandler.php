<?php session_start();
require_once('../utility/dbFunctions.php');
require_once('../utility/phpFunctions.php');

/*
//Project Name: GCU Student Blog
//Version 1.4
//Module: Blog Search Handler Version 1
//Programmers: Robbie Evans III, Michael Rogers
//Date: 8/27/2017
//Synopsis: Search Blogs Handler
//Requires dbFunctions.php, phpFunctions.php, searchError.php, _searchResults.php
*/


$searchFirstName = $_POST['firstname'];
$searchLastName = $_POST['lastname'];
$searchTitle = $_POST['title'];
$searchDate = $_POST['date'];

$mysqli = dbConnect();

if ($searchFirstName == null && $searchLastName == null && $searchTitle == null && $searchDate == null) {
    $message = "Please enter a search parameter.";
    include('searchError.php');
}
elseif (!$searchFirstName == null && !$searchLastName == null) {
    $message = "Please use only one search parameter.";
    include('searchError.php');
}
elseif (!$searchFirstName == null && !$searchTitle == null) {
    $message = "Please use only one search parameter.";
    include('searchError.php');
}
elseif (!$searchFirstName == null && !$searchDate == null) {
    $message = "Please use only one search parameter.";
    include('searchError.php');
}
elseif (!$searchLastName == null && !$searchTitle == null) {
    $message = "Please use only one search parameter.";
    include('searchError.php');
}
elseif (!$searchLastName == null && !$searchDate == null) {
    $message = "Please use only one search parameter.";
    include('searchError.php');
}
elseif (!$searchTitle == null && !$searchDate == null) {
    $message = "Please use only one search parameter.";
    include('searchError.php');
}
elseif (!$searchFirstName == null) {
    $results = searchAuthorFirstName($searchFirstName);
    if (count($results) > 25){
        $message = "Too many results. Please refine your search.";
        include('searchError.php');
    }
    else {
        include('_searchResults.php');
    }
}
elseif (!$searchLastName == null) {
    $results = searchAuthorLastName($searchLastName);
    if (count($results) > 25){
        $message = "Too many results. Please refine your search.";
        include('searchError.php');
    }
    else {
        include('_searchResults.php');
    }
}
elseif (!$searchTitle == null) {
    $results = searchTitles($searchTitle);
    if (count($results) > 25){
        $message = "Too many results. Please refine your search.";
        include('searchError.php');
    }
    else {
        include('_searchResults.php');
    }
}
elseif (!$searchDate == null) {
    $results = searchDates($searchDate);
    if (count($results) > 25){
        $message = "Too many results. Please refine your search.";
        include('searchError.php');
    }
    else {
        include('_searchResults.php');
    }
}


$mysqli->close();
?>