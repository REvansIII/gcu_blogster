<?php session_start();

include('blogHeader.php');
include('_blogMenu.php');
require_once('../utility/dbFunctions.php');

/*
//Project Name: GCU Student Blog
//Version 1.3
//Module: Blog My Blogs Page Version 1
//Programmers: Robbie Evans III, Michael Rogers
//Date: 8/20/2017
//Synopsis: Page for user to see list of their own blogs.
//Requires blogHeader.php, _blogMenu.php, utility/dbFunctions.php, Blog_Viewer.php, Blog_Update.php, Blog_Delete.php
*/

$ID = $_SESSION["USER_ID"];

// connection to database
$mysqli = dbConnect();

// data from blog entries table
$data = $mysqli->query ("SELECT * FROM entries where UserId = '$ID'");

//Display table with blog information
echo "<table border = '1' align='center'>
    <tr>
        <th>Blog Title</th>
        <th>Date Published</th>
    </tr>";

//Iterate array to populate table
foreach ($data as $row)
{
    echo "<tr>";
    echo "<td>" . $row['Title'] . "</td>";
    echo "<td>" . $row['Date'] . "</td>";
    echo "<td><a href=\"Blog_Viewer.php?BlogID=" . $row['BlogID'] . "\"> View </a></td>";
    echo "<td><a href=\"Blog_Update.php?BlogID=" . $row['BlogID'] . "\"> Update </a></td>";
    echo "<td><a href=\"Blog_Delete.php?BlogID=" . $row['BlogID'] . "\"> Delete </a></td>";
    echo "</tr>";
}
echo "</table>";

$mysqli->close();

?>