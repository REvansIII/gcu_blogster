<?php session_start();
include('blogHeader.php');
include('_blogMenu.php');
require_once('../utility/dbFunctions.php');
require_once('../utility/phpFunctions.php');
?>

<!--
//Project Name: GCU Student Blog
//Version 1.5
//Module: Blog Search Results Version 1
//Version 1.5 Updates: Removed table generating code and replaced it with displayTable function.
//Programmers: Robbie Evans III, Michael Rogers
//Date: 9/10/2017
//Synopsis: Displays results from user search
-->

    <?php
    if(count($results) == 0 ){  //Display message if no search results are found.
        $message = "No results found.";
        include('searchResultsNone.php');
    }
    else {
        displayTable($results);
    }
    ?>
