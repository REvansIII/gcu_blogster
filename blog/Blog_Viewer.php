<?php session_start();


include('blogHeader.php');
include('_blogMenu.php');
require_once('../utility/dbFunctions.php');

/*
//Project Name: GCU Student Blog
//Version 1.5
//Module: Blog Viewer Page Version 1
//Version 1.4 Updates: Added nl2br() function to properly display blogs entries. Added ability to comment on blogs
//                     Added display of all comments associated with blog entry.
//Version 1.5 Updates: Removed Post Comment form from appearing on View page if user is the entry creator.
//                     Added wrapper div class to better align all the forms.
//Programmers: Robbie Evans III, Michael Rogers
//Date: 9/10/2017
//Synopsis: Page for a user to view and read a blog entry.
//Requires blogHeader.php, _blogMenu.php, utility/dbFunctions.php
*/


$ID = $_GET['BlogID'];

//Connect to DB
$mysqli = dbConnect();

//Get entry information from database
$blogPost = $mysqli->query ("SELECT * FROM entries WHERE BlogID = '$ID'");
$result = $blogPost->fetch_array(MYSQLI_ASSOC);
//Get comment information from database
$comments = $mysqli->query ("SELECT userinfo.FirstName, userinfo.LastName, comments.Comment,
                            comments.CommentDate FROM userinfo INNER JOIN comments ON userinfo.ID = comments.CommenterID
                            WHERE BlogEntryID = '$ID'");

echo "<div id = 'wrapper'>";

//Display title and content of blog.
foreach ($blogPost as $row)
{
    echo "<div class = 'blogViewer'>";
    echo "<table width='100%'>";
    echo "<tr>";
    echo "<td><span style = 'font-weight: bold;'>" . 'Title: ' . $row['Title'] . "</span>
    <span style = 'font-weight: bold; float: right;'>" . 'Date Posted: ' . $row['Date'] . "</span></td><br><br>";
    echo "</tr>";
    echo "<tr>";
    echo "<td> " . nl2br($row['Entry']) . "</td>";
    echo "</tr>";
    echo "</table>";
    echo "</div>";
}

//Display form to post a comment on the blog entry if blog does not belong to current user.
    echo "<br><br>";
    echo "<div class = 'commentForm'>";

if ($_SESSION["USER_ID"] != $result["UserId"]) {
    echo "<form id = 'commentForm' action = commentHandler.php method = 'post'>";
    echo "<textarea name = 'comment' rows = '15' cols= '50' placeholder='Post A Comment' required/></textarea>
    <textarea name = 'BlogId' rows ='1' cols = '1' hidden>" . $ID . "</textarea><br>";
    echo "<input type= 'submit' value= 'Post Comment' style = 'width:110px;'>";
    echo "</form>";
}

//Display all comments associated with blog entry.
foreach ($comments as $row)
{
    echo "<table width='380'>";
    echo "<tr>";
    echo "<td><span style = 'font-weight: bold;'>" . 'Comment By: ' . $row['FirstName'] . " " . $row['LastName'] . "</span>
    <span style = 'font-weight: bold; float: right;'>" . 'Date: ' . $row['CommentDate'] . "</span></td><br>";
    echo "</tr>";
    echo "<tr>";
    echo "<td> " . $row['Comment'] . "</td>";
    echo "</tr>";

}
echo "</table>";
echo "</div>";

echo "</div>";

$mysqli->close();
?>
