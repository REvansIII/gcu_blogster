<?php session_start();
/*
//Project Name: GCU Student Blog
//Version 1.5
//Module: Blog Comment Handler Page Version 1
//Version 1.5 Updates: Changed SQL statement to a prepared statement.
//Programmers: Robbie Evans III, Michael Rogers
//Date: 9/10/2017
//Synopsis: Handler for posting comments to blogs.
//Requires blogHeader.php, _blogMenu.php, utility/dbFunctions.php, postResults.php, Blog_Viewer.php
*/

require_once('../utility/dbFunctions.php');

$comment = $_POST['comment'];
$ID = $_POST['BlogId'];

//Connect to DB
$mysqli = dbConnect();

//Validate that fields contain data.
if (strlen($_POST['comment']) == null)
{
    $message = "Error! Your comment was blank.";
    include('postResults.php');
}

else {
    //Insert comment into DB using prepared statement
    $stmt = $mysqli->prepare("INSERT INTO comments (Comment, CommentDate, CommenterId, BlogEntryID) 
                                    VALUES (?, Now(), ?, ?)");
    $stmt->bind_param("sss", $comment, $_SESSION["USER_ID"], $ID);
    $stmt->execute();
    $stmt->close();
    $message = "Comment has been submitted.";
    include('postResults.php');
}

$mysqli->close();

?>