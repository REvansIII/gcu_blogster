<?php session_start();

include('blogHeader.php');
include('_blogMenu.php');
require_once('../utility/dbFunctions.php');

/*
//Project Name: GCU Student Blog
//Version 1.3
//Module: Blog Update Page Version 1
//Programmers: Robbie Evans III, Michael Rogers
//Date: 8/20/2017
//Synopsis: Page for user to update one of their own selected blogs.
//Requires blogHeader.php, _blogMenu.php, utility/dbFunctions.php, updateHandler.php
*/


$ID = $_GET['BlogID'];

//Connect to DB
$mysqli = dbConnect();

//Get entry information from database
$blogPost = $mysqli->query ("SELECT * FROM entries WHERE BlogID = '$ID'");

//Display current blog entry information. User can modify title/entry and submit changes to database.
//Blog ID is stored in a hidden field so that it transfers via POST on submission
while($row = mysqli_fetch_array($blogPost))
{
    echo "<div class = 'blogForm'>";
    echo "<form id = 'blogForm' action = updateHandler.php method = 'post'>";
    echo "Title: <textarea name = 'title' rows = '1'  cols= '60' maxlength = '100' required/>" . $row['Title'] .
        "</textarea>
         <textarea name = 'BlogId' rows ='1' cols = '1' hidden>" . $row['BlogID'] . "</textarea>";
    echo "Blog Entry: <textarea name = 'entry' rows = '20'  cols= '60' required/>" . $row['Entry'] . "</textarea>";
    echo "<input type= 'submit' value= 'Submit'>";
    echo "</form>";
    echo "</div>";
}

$mysqli->close();