<?php session_start();

include('blogHeader.php');
include('_blogMenu.php');
require_once('../utility/dbFunctions.php');

/*
//Project Name: GCU Student Blog
//Version 1.4
//Module: Blog Confirm Delete Handler Version 1
//Version 1.4 Updates: Updated Delete command to remove comments associated with blog entry
//Programmers: Robbie Evans III, Michael Rogers
//Date: 9/3/2017
//Synopsis: Handler for deletion of selected blogs.
//Requires blogHeader.php, _blogMenu.php, utility/dbFunctions.php, Blog_Delete.php
*/



$ID = $_GET['BlogID'];
$message = "Entry has been deleted";

//Connect to DB
$mysqli = dbConnect();

//Determine if blog has any associated comments
$sql = $mysqli->query ("SELECT * FROM comments WHERE BlogEntryID = '$ID'");
$count = $sql->num_rows;

//Delete blog entry and associated comments from database and display success message to user.
if($count > 0) {
    $blogPost = $mysqli->query("DELETE entries, comments FROM entries INNER JOIN comments
                                    ON entries.BlogID = comments.BlogEntryID WHERE BlogID = '$ID'");
}
//Execute if blog entry has no associated comments and display success message to user.
else {
    $blogPost = $mysqli->query ("DELETE FROM entries WHERE BlogID = '$ID'");
}
$mysqli->close();
?>
<h2><?php echo $message ?></h2><br>
