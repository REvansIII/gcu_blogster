<!--
//Project Name: GCU Student Blog
//Version 1.4
//Module: Blog Post Results Page Version 1
//Programmers: Robbie Evans III, Michael Rogers
//Date: 8/27/2017
//Synopsis: Page to display no results found from blog search
//Requires searchBlog.php
-->


<h2><?php echo $message ?></h2><br>
<a href="searchBlog.php"><p style="text-align:center">Back to Search</p></a>


</body>
</html>