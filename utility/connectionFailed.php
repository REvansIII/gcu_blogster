<?php session_start(); ?>
<!--
//Project Name: GCU Student Blog
//Version 1.3
//Module: Connection Failed Version 1
//Programmers: Robbie Evans III, Michael Rogers
//Date: 8/20/2017
//Synopsis: Page to display connection failure message based on passed in variable.
-->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Database Connection Failed</title>
</head>
<body>
<h2><?php echo $message ?></h2><br>
<a href = "index.html">Return to Homepage.</a>


</body>
</html>