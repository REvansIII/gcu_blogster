<?php
/*
//Project Name: GCU Student Blog
//Version 1.3
//Module: Database Functions Version 1.0
//Programmers: Robbie Evans III, Michael Rogers
//Date: 8/20/2017
//Synopsis: Stores functions related to working with database.
*/


//Establish connection to database.
function dbConnect(){
    $connection = new mysqli("localhost", "root", "root" , "blogusers");
    if ($connection->connect_errno)
    {
        $message = "Connection Failed.";
        include('connectionFailed.php');
    }
    else
    {
        return $connection;
    }
}



?>