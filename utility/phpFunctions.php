<?php
/*
//Project Name: GCU Student Blog
//Version 1.5
//Module: PHP Functions Version 1
//Version 1.4 Updates - Added Search functions
Version 1.5 Updates: Added displayAllBlogs() and displayTable() functions.
//Programmers: Robbie Evans III, Michael Rogers
//Date: 9/10/2017
//Synopsis: Stores PHP functions for use in other forms.
*/

//Save User ID in the Session
function saveUserId($id)
{
    session_start();
    $_SESSION["USER_ID"] = $id;
}

//Retrieve User ID from the Session
function getUserId()
{
    session_start();
    return $_SESSION["USER_ID"];
}

//Validate that input field is not blank
function validateInput($input)
{
    if ($input == NULL || trim($input) == "" )
        return true;
}

//Search for all Blogs
function displayAllBlogs() {
    $mysqli = dbConnect();
    $stmt = $mysqli->prepare("SELECT userinfo.ID, userinfo.FirstName, userinfo.LastName, entries.Title, entries.BlogID,
                               entries.Date FROM userinfo INNER JOIN entries ON userinfo.ID = entries.UserId");
    $stmt->execute();
    $stmt->bind_result($ID, $FirstName, $LastName, $Title, $BlogID, $Date);
    $rows = array();
    $count = 0;

    while ($stmt->fetch())
    {
        $rows[$count] = array($FirstName, $LastName, $Title, $Date, $BlogID);
        $count++;
    }
    $mysqli->close();
    return $rows;
}

//Searches database for Titles similar to user's input
function searchTitles($pattern)
{
    $param = '%' . $pattern . '%';
    $mysqli = dbConnect();
    $stmt = $mysqli->prepare("SELECT userinfo.ID, userinfo.FirstName, userinfo.LastName, entries.Title, entries.BlogID,
                               entries.Date FROM userinfo INNER JOIN entries ON userinfo.ID = entries.UserId
                               WHERE entries.Title LIKE ?");
    $stmt->bind_param("s", $param);
    $stmt->execute();
    $stmt->bind_result($ID, $FirstName, $LastName, $Title, $BlogID, $Date);
    $title = array();
    $count = 0;

    while ($stmt->fetch())
    {
        $title[$count] = array($FirstName, $LastName, $Title, $Date, $BlogID);
        $count++;
    }
    $mysqli->close();
    return $title;
}

//Searches database for Dates similar to user's input
function searchDates($pattern)
{
    $param = '%' . $pattern . '%';
    $mysqli = dbConnect();
    $stmt = $mysqli->prepare("SELECT userinfo.ID, userinfo.FirstName, userinfo.LastName, entries.Title, entries.BlogID,
                               entries.Date FROM userinfo INNER JOIN entries ON userinfo.ID = entries.UserId
                               WHERE entries.Date LIKE ?");
    $stmt->bind_param("s", $param);
    $stmt->execute();
    $stmt->bind_result($ID, $FirstName, $LastName, $Title, $BlogID, $Date);
    $date = array();
    $count = 0;

    while ($stmt->fetch())
    {
        $date[$count] = array($FirstName, $LastName, $Title, $Date, $BlogID);
        $count++;
    }
    $mysqli->close();
    return $date;
}

//Searches database for First Name similar to user's input
function searchAuthorFirstName($pattern)
{
    $param = '%' . $pattern . '%';
    $mysqli = dbConnect();
    $stmt = $mysqli->prepare("SELECT userinfo.ID, userinfo.FirstName, userinfo.LastName, entries.Title, entries.BlogID,
                               entries.Date FROM userinfo INNER JOIN entries ON userinfo.ID = entries.UserId
                               WHERE userinfo.FirstName LIKE ?");
    $stmt->bind_param("s", $param);
    $stmt->execute();
    $stmt->bind_result($ID, $FirstName, $LastName, $Title, $BlogID, $Date);
    $firstName = array();
    $count = 0;

    while ($stmt->fetch())
    {
        $firstName[$count] = array($FirstName, $LastName, $Title, $Date, $BlogID);
        $count++;
    }
    $mysqli->close();
    return $firstName;
}

//Searches database for Last Name similar to user's input
function searchAuthorLastName($pattern)
{
    $param = '%' . $pattern . '%';
    $mysqli = dbConnect();
    $stmt = $mysqli->prepare("SELECT userinfo.ID, userinfo.FirstName, userinfo.LastName, entries.Title, entries.BlogID,
                               entries.Date FROM userinfo INNER JOIN entries ON userinfo.ID = entries.UserId
                               WHERE userinfo.LastName LIKE ?");
    $stmt->bind_param("s", $param);
    $stmt->execute();
    $stmt->bind_result($ID, $FirstName, $LastName, $Title, $BlogID, $Date);
    $lastName = array();
    $count = 0;

    while ($stmt->fetch())
    {
        $lastName[$count] = array($FirstName, $LastName, $Title, $Date, $BlogID);
        $count++;
    }
    $mysqli->close();
    return $lastName;
}


function displayTable($array) {

    echo "<table border = '1' align='center'>
    <tr>
        <th>Blog Title Test</th>
        <th>Author Name</th>
        <th>Date Published</th>
    </tr>";

//Iterate over array and display relevant blog information.
    //Populate table with results of search
    for($x = 0; $x < count($array); $x++) {
        echo "<tr>";
        echo "<td>" . $array[$x][2] . "</td>" .
            "<td>" . $array[$x][0] . " " . $array[$x][1] . "</td>" .
            "<td>" . $array[$x][3] . "</td>";
        echo "<td><a href=\"../blog/Blog_Viewer.php?BlogID=" . $array[$x][4] . "\"> View </a></td>";
        echo "</tr>";
    }
    echo "</table>";
}

?>