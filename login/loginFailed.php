<?php session_start();

include('loginHeader.php');
?>

<!--
//Project Name: GCU Student Blog
//Version 1.3
//Module: Login Failed Version 1.0
//Programmers: Robbie Evans III, Michael Rogers
//Date: 8/20/2017
//Synopsis: Page to display login failure message based on passed in variable.
//Requires: loginHeader.php, login.php
-->


<h3><?php echo $message ?></h3><br>
<p style = "text-align: center">
    <a href = "login.php">Return to Login Page.</a>
</p>
</body>
</html>