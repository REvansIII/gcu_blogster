<?php session_start();
/*
//Project Name: GCU Student Blog
//Version 1.5
//Module: Login Handler Version 1.1
//Version 1.1: Added function to connect to database. Added loginFailed.php functionality.
//Version 1.2: Added check to see if all fields have input.
Version 1.5 Updates: Changed SQL statement to a prepared statement.
//Programmers: Robbie Evans III, Michael Rogers
//Date: 9/10/2017
//Synopsis: Retrieves login input from login.php. Query database to see if info exists and matches. Grants/denies
            access depending on match.
//Requires login.php, utility/dbFunctions.php, utility/phpFunctions.php, loginFailed.php, blog/Blog.php
*/

require_once('../utility/dbFunctions.php');
require_once('../utility/phpFunctions.php');

$userName = $_POST['username'];
$password = $_POST['password'];

//Establish connection to database
$mysqli = dbConnect();

//Prepared statement to check database for login credentials
$sql = $mysqli->prepare("SELECT ID, EmailAddress, Password FROM userinfo WHERE EmailAddress = ? AND Password = ?");
$sql->bind_param("ss", $userName, $password );
$sql->execute();
$sql->bind_result($id, $userName, $password);
$sql->store_result();


//Check if any fields are empty
if (validateInput($userName) || validateInput($password))
{
    $message = "Both fields are required.";
    include('loginFailed.php');
}
//Grant access if 1 row is found, deny if 0 rows or multiple rows are found
elseif ($sql->num_rows == 1)
{
    while($sql->fetch()) {
        saveUserId($id);
        include('../blog/Blog.php');;
    }
}
elseif ($sql->num_rows == 0)
{
    $message = "Invalid Credentials.";
    include('loginFailed.php');
}
elseif ($sql->num_rows > 1)
{
    $message = "There are multiple users that have registered these credentials.";
    include('loginFailed.php');
}
else
{
    $message = "Connection failed: " . $mysqli->connect_errno;
    include('loginFailed.php');
}


$mysqli->close();
?>
