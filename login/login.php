<?php session_start();

include('loginHeader.php');
?>

<!--
//Project Name: GCU Student Blog
//Version 1.3
//Module: Login Page Version 1
//Programmers: Robbie Evans III, Michael Rogers
//Version 1.3 Updates: Converted from HTML to php extension.
//Date: 8/20/2017
//Synopsis: Display fields for user to login to blog account. Validates input against database and grants/denies access.
//Requires loginHandler.php, loginHeader.php, and style.css
-->


<!-- Display Login Form -->
<div class = "loginForm">
<form id="loginForm" action = loginHandler.php method="post">
    <label>Email Address</label><br>
    <input name="username" type="text"  maxlength="255" required/><br>
    <label>Password</label><br>
    <input name="password" type="password" maxlength="15" required/><br>
    <input name="Submit" value="Login" type="submit"/><br><br>
    <a href="../registration/registration.php">Go Back to Registration.</a>

</form>
</div>


</body>
</html>