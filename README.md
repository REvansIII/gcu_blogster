# README #

Software Description
The GCU Blogster webpage allow students to share their student experience upon registration.  As of 30July2017 the webpage consists of a registration page(html) that allows the user to register with the service (via a php file named registrationHandler) and a login page(html), that once the user is registered, allows the user to log in(via a php file named loginHandler) to the service. A database holds a table of users which the login page references to authenticate if a user is registered.
Potential revisions include design improvements to make the site aesthetically pleasing to the client.
8/6/2017 – Added Blog Entry Page. User can now post a blog entry and save it to the database. Optimized existing code.
8/13/2017 – Removed Echos. Added additional error handling. Added maxlength properties to title and entry forms. On Blog_Entry.php. Added index page. Added check to make sure username and password fields contained input on login page. Added query to check if email address already exists in database to registrationHandler. Created logo for website.
Modules/Files
1.	_blogMenu.html
2.	Blog.html
3.	Blog_Entry.html
4.	blogHandler.php
5.	blogPosted.php
6.	blogSearch.html
7.	connectionFailed.php
8.	dbFunctions.php
9.	errorHandler.php
10.	index.php
11.	login.html
12.	loginFailed.php
13.	loginHandler.php
14.	logo.jpg
15.	phpFunctions.php
16.	registerFailed.php
17.	registerSuccess.php
18.	registration.html
19.	registrationHandler.php
20.	style.css
User Guide
1.	Register on the registration page.
2.	Once registered, login using username(email) and password
3.	At Blog home screen, click Create New Blog.
4.	Enter title, blog entry, and click submit.
