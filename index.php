<?php
/*
//Project Name: GCU Student Blog
//Version 1.3
//Module: Registration HTML Page Version 1
//Programmers: Robbie Evans III, Michael Rogers
//Date: 8/20/2017
//Synopsis: Initial page to be displayed to visitors.
//Requires registrationHandler.php, login.php, and style.css
*/
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>GCU Student Blog</title>
    <link rel="stylesheet" href="style.css"/>

</head>

<body link="#00bfff" vlink="red">
<div class="header">
    <img src="logo.jpg" alt="logo" />
    <h1>GCU Student Blog</h1>
</div>

<div class = "homeForm">
    <form id="homeForm">
        New User?<br>
        <a href="registration/registration.php">Click here to register.</a><br><br>
        Already registered?<br>
        <a href="login/login.php">Click here to login.</a>

    </form>
</div>

</body>
</html>